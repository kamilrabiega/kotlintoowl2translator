# KotlinToOWL2Translator
### Plugin which translate _Kotlin_ source code into _OWL2_ ontology. Created for _OWL2 Universal Framework Processor_.

---

To use it, simply:

* Build with Maven: ```mvn clean install```


* Copty artifact from _**target/kotlin-to-owl2-translator-0.0.1-SNAPSHOT**_ ...
* ... into _**translate**_ directory of _UPF_
* Start translating your source code 

---

_OWL2 Universal Framework Processor_ is available **[here](https://bitbucket.org/pworoniecki/owl2-converter/src/master/)**.