package owl2converter.model.kotlinclass;

public enum ClassModifier {
	ABSTRACT, INNER, OPEN, SEALED, DATA, ENUM
}
