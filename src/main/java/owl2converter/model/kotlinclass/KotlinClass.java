package owl2converter.model.kotlinclass;

import java.util.ArrayList;
import java.util.List;

import owl2converter.model.field.Field;

public class KotlinClass {
	
	private String name;
	private ClassModifier modifier;
	private KotlinClass inheritedClass;
	private KotlinClass outerClass;
	private List<Field> fields;
	private boolean isClassAlreadyVisited;
	private List<String> entries;

	public KotlinClass() {
		fields = new ArrayList<>();
		entries = new ArrayList<>();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public List<Field> getFields() {
		return fields;
	}
	public void setFields(List<Field> fields) {
		this.fields = fields;
	}
	
	public ClassModifier getModifier() {
		return modifier;
	}
	public void setModifier(ClassModifier modifier) {
		this.modifier = modifier;
	}
	
	public KotlinClass getInheritedClass() {
		return inheritedClass;
	}
	public void setInheritedClass(KotlinClass inheritedClass) {
		this.inheritedClass = inheritedClass;
	}
	
	public boolean isClassAlreadyVisited() {
		return isClassAlreadyVisited;
	}
	public void setClassAlreadyVisited(boolean isClassAlreadyVisited) {
		this.isClassAlreadyVisited = isClassAlreadyVisited;
	}
	
	public List<String> getEntries() {
		return entries;
	}

	public void setEntries(List<String> entries) {
		this.entries = entries;
	}
	
	public KotlinClass getOuterClass() {
		return outerClass;
	}

	public void setOuterClass(KotlinClass outerClass) {
		this.outerClass = outerClass;
	}
	
	@Override
	public String toString() {
		return "KotlinClass [name=" + name + ", modifier=" + modifier 
				+ ", inheritedClass=" + inheritedClass
				+ ", outerClass=" + outerClass
				+ ", fields=" + fields
				+ ", entries=" + entries 
				+ ", visited=" + isClassAlreadyVisited + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		KotlinClass other = (KotlinClass) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}
