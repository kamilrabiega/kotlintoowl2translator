package owl2converter.model.field.type;

public enum Collection {
	LIST, MUTABLELIST, SET, MUTABLESET, MAP, MUTABLEMAP;
	
	public static boolean contains(String type) {
		for(Collection collection : Collection.values()) {
			if(collection.name().equalsIgnoreCase(type)) {
				return true;
			}
		}
		return false;
	}
}
