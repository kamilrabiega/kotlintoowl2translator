package owl2converter.model.field.type;

public enum BasicType {
	
	BYTE, SHORT, INT, LONG, FLOAT, DOUBLE, BIGDECIMAL, BOOLEAN, STRING;
	
	public static boolean contains(String type) {
		for(BasicType basicType : BasicType.values()) {
			if(basicType.name().equalsIgnoreCase(type)) {
				return true;
			}
		}
		return false;
	}
	
}
