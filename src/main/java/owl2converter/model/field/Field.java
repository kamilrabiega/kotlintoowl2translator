package owl2converter.model.field;

public class Field {
	
	private String name;
	private String valueType;
	private boolean isCollection;
	
	public Field() {}
	
	public Field(String name, String valueType, boolean isCollection) {
		super();
		this.name = name;
		this.valueType = valueType;
		this.isCollection = isCollection;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getValueType() {
		return valueType;
	}
	public void setValueType(String valueType) {
		this.valueType = valueType;
	}
	public boolean isCollection() {
		return isCollection;
	}
	public void setCollection(boolean isCollection) {
		this.isCollection = isCollection;
	}
	
	@Override
	public String toString() {
		return "Field [name=" + name + ", valueType=" + valueType + ", isCollection=" + isCollection + "]";
	}
	
}
