package owl2converter.parser;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import com.github.sarahbuisson.kotlinparser.KotlinLexer;
import com.github.sarahbuisson.kotlinparser.KotlinParser;

import owl2converter.model.kotlinclass.KotlinClass;
import owl2converter.parser.listeners.ClassWalkerListener;

public class Parser {
	
	public static void parse(List<KotlinClass> classRepository, String fileContent) throws IOException {
		
		InputStream inputStream = new ByteArrayInputStream(fileContent.getBytes());
		
		KotlinLexer KotlinLexer = new KotlinLexer(
				CharStreams.fromStream(inputStream)
		);
		
	    CommonTokenStream commonTokenStream = new CommonTokenStream(KotlinLexer);
	    KotlinParser kotlinParser = new KotlinParser(commonTokenStream);

	    ParseTree tree = kotlinParser.kotlinFile();
	    ParseTreeWalker walker = new ParseTreeWalker();
	    
	    ClassWalkerListener listener = new ClassWalkerListener(classRepository);

	    walker.walk(listener, tree);
	    
	}
	
}
