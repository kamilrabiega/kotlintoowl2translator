package owl2converter.parser.listeners;

import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;

import com.github.sarahbuisson.kotlinparser.KotlinParser;
import com.github.sarahbuisson.kotlinparser.KotlinParserBaseListener;

import owl2converter.helpers.ParserHelper;
import owl2converter.model.field.Field;
import owl2converter.model.field.type.Collection;
import owl2converter.model.kotlinclass.ClassModifier;
import owl2converter.model.kotlinclass.KotlinClass;

public class ClassWalkerListener extends KotlinParserBaseListener {
	
	private List<KotlinClass> classRepository;
	
	public ClassWalkerListener(List<KotlinClass> classRepository) {
		this.classRepository = classRepository;
	}

    @Override
    public void enterClassDeclaration(KotlinParser.ClassDeclarationContext ctx) {
        String classModifier = "";
        String className = "";
        String parentClassName = "";
        for(int i=0; i<ctx.getChildCount(); i++) {
        	if(ctx.getChild(i).getText().equals("class")) {
        		className = ctx.getChild(i+1).getText();
        		if(i>0) {
        			classModifier = ctx.getChild(i-1).getText();
        		}
        	}
        	if(ctx.getChild(i).getText().equals(":") && ctx.getChild(i+1).getText().contains("(")) {
        		parentClassName = ctx.getChild(i+1).getText();
        		parentClassName = parentClassName.substring(0, parentClassName.indexOf('('));
        	}
        }
        
        KotlinClass kotlinClass = new KotlinClass();
        KotlinClass kotlinParentClass = new KotlinClass();
        if(!className.isEmpty()) {
        	kotlinClass.setName(className);
        }
        if(!classModifier.isEmpty()) {
        	kotlinClass.setModifier(ClassModifier.valueOf(classModifier.toUpperCase()));
        }
        if(!parentClassName.isEmpty()) {
        	kotlinParentClass.setName(parentClassName);
        }
        
        if(kotlinParentClass.getName() != null ) {
	        if(classRepository.contains(kotlinParentClass)) {
	        	int index = classRepository.indexOf(kotlinParentClass);
	        	if(!classRepository.get(index).isClassAlreadyVisited()) {
	        		classRepository.set(index, kotlinParentClass);
	        	}
	        }
	        else {
	        	classRepository.add(kotlinParentClass);
	        }
        }
        
        for(KotlinClass classFromRepository : classRepository) {
        	if(classFromRepository.getName().equals(parentClassName)) {
        		kotlinClass.setInheritedClass(classFromRepository);
        	}
        }
        
        if(kotlinClass.getName()!=null && classRepository.contains(kotlinClass)) {
        	int index = classRepository.indexOf(kotlinClass);
        	if(!classRepository.get(index).isClassAlreadyVisited()) {
        		kotlinClass.setClassAlreadyVisited(true);
        		classRepository.set(index, kotlinClass);
        	}
        }
        else if(kotlinClass.getName() != null) {
        	kotlinClass.setClassAlreadyVisited(true);
        	classRepository.add(kotlinClass);
        }
        
        String outerClassName = "";
        if(kotlinClass.getModifier() !=null && kotlinClass.getModifier().equals(ClassModifier.INNER)) {
        	outerClassName = ParserHelper.findClass(ctx);
        }
        if(!outerClassName.isEmpty()) {
        	for(KotlinClass classFromRepository : classRepository) {
        		if(classFromRepository.getName().equals(outerClassName)) {
        			kotlinClass.setOuterClass(classFromRepository);
        		}
        	}
        }
    }
    
    @Override
    public void enterClassParameter(KotlinParser.ClassParameterContext ctx) {
    	
    	String classParameterName = "";
    	String classParameterClass = "";
    	String classParameterType = "";
    	
    	for(int i=0; i<ctx.getChildCount(); i++) {
    		if(ctx.getChild(i).getText().equals("var")) {
    			classParameterName = ctx.getChild(i+1).getText();
    			classParameterType = ctx.getChild(i+3).getText();
    		}
    		if(ctx.getChild(i).getText().equals("val")) {
    			classParameterName = ctx.getChild(i+1).getText();
    			classParameterType = ctx.getChild(i+3).getText();
    		}
    	}
    	
    	for(int i=0; i<ctx.getParent().getParent().getParent().getChildCount(); i++) {
    		if(ctx.getParent().getParent().getParent().getChild(i).getText().equals("class")) {
    			classParameterClass = ctx.getParent().getParent().getParent().getChild(i+1).getText();
    		}
    	}
    	
    	Field field = new Field();
    	if(!classParameterName.isEmpty()) {
    		field.setName(classParameterName);
    		field.setCollection(ParserHelper.isCollection(classParameterType));
    		field.setValueType(ParserHelper.convertType(classParameterType));
    		for(KotlinClass classFromRepository : classRepository) {
            	if(classFromRepository.getName().equals(classParameterClass)) {
            		classFromRepository.getFields().add(field);
            	}
            }
    	}
    }
    
    @Override
    public void enterEnumEntry(KotlinParser.EnumEntryContext ctx) {
    	String entry = ctx.getChild(0).getText();
    	String entryClass = ParserHelper.findClass(ctx);
    	for(KotlinClass classFromRepository : classRepository) {
        	if(classFromRepository.getName().equals(entryClass)) {
        		classFromRepository.getEntries().add(entry);
        	}
        }
    }
    
    @Override
    public void enterPropertyDeclaration(KotlinParser.PropertyDeclarationContext ctx) {
    	String classPropertyName = "";
    	String classPropertyClass = "";
    	String classPropertyType = "";
    	
    	for(int i=0; i<ctx.getChildCount(); i++) {
    		if(ctx.getChild(i).getText().equals("var")) {
    			classPropertyName = ctx.getChild(i+1).getChild(0).getText();
    			classPropertyType = ctx.getChild(i+1).getChild(2).getText();
    		}
    		if(ctx.getChild(i).getText().equals("val")) {
    			classPropertyName = ctx.getChild(i+1).getChild(0).getText();
    			classPropertyType = ctx.getChild(i+1).getChild(2).getText();
    		}
    	}
    	
    	classPropertyClass = ParserHelper.findClass(ctx);
    	
    	Field field = new Field();
    	if(!classPropertyName.isEmpty()) {
    		field.setName(classPropertyName);
    		field.setCollection(ParserHelper.isCollection(classPropertyType));
    		field.setValueType(ParserHelper.convertType(classPropertyType));
    		for(KotlinClass classFromRepository : classRepository) {
            	if(classFromRepository.getName().equals(classPropertyClass)) {
            		classFromRepository.getFields().add(field);
            	}
            }
    	}
    	
    }

	public List<KotlinClass> getClassRepository() {
		return classRepository;
	}

	public void setClassRepository(List<KotlinClass> classRepository) {
		this.classRepository = classRepository;
	}
    
}
