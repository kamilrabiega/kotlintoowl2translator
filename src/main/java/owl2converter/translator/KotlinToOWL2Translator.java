package owl2converter.translator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import commoncodemetamodel.Element;
import owl2converter.generator.OntologyGenerator;
import owl2converter.helpers.TranslatorHelper;
import owl2converter.model.kotlinclass.KotlinClass;
import toowl2builderapi.ToOWL2Translator;

@Component("translatorComponent")
public class KotlinToOWL2Translator extends ToOWL2Translator {
	private static final Logger logger = LoggerFactory.getLogger(KotlinToOWL2Translator.class);
	private static final String SOURCE_LANG_NAME = "Kotlin";
	private static final String ONTOLOGY_IRI = "http://example.com/ontology";
	private static final OWLOntologyManager ontologyManager = OWLManager.createConcurrentOWLOntologyManager();
	private static final OWLDataFactory owlDataFactory = ontologyManager.getOWLDataFactory();
	
	@Override
	public OWLOntology translateCodeToOntology(Set<Element> elements) {
		
		long startTime = System.currentTimeMillis();
		logger.info("Translation has been started");
		
		List<KotlinClass> classList = new ArrayList<>();
		TranslatorHelper.listOutAllKotlinFilesFormCommonCodeMetamodel(classList, elements);
		logger.info("Parsed Kotlin classes: "+classList.size());
		Collection<OWLAxiom> axioms = OntologyGenerator.generateOntologyFromClasses(classList, owlDataFactory);
		
		long endTime = System.currentTimeMillis();
		logger.info("Translation completed in: "+ (endTime - startTime) +"ms");
		logger.info("Generated axioms: "+axioms.size());
		try {
			if(ontologyManager.contains(IRI.create(ONTOLOGY_IRI))) {
				ontologyManager.clearOntologies();
			}
			return ontologyManager.createOntology(axioms, IRI.create(ONTOLOGY_IRI));
		} catch (OWLOntologyCreationException e) {
			logger.error(e.toString());
		}
		return null;
	}
	
	@Override
	public String getSourceLanguageName() {
		return SOURCE_LANG_NAME;
	}

}