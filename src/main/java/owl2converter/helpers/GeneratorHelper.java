package owl2converter.helpers;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owlapi.vocab.OWL2Datatype;

import owl2converter.model.field.type.BasicType;
import owl2converter.model.kotlinclass.ClassModifier;
import owl2converter.model.kotlinclass.KotlinClass;

public class GeneratorHelper {
	
	public static OWL2Datatype convertBasicType(BasicType kotlinType) {
		OWL2Datatype owlType;
		
		switch(kotlinType) {
			case BYTE: {
				owlType = OWL2Datatype.XSD_BYTE;
				break;
			}
			case SHORT: {
				owlType = OWL2Datatype.XSD_SHORT;
				break;
			}
			case INT: {
				owlType = OWL2Datatype.XSD_INT;
				break;
			}
			case LONG: {
				owlType = OWL2Datatype.XSD_LONG;
				break;
			}
			case FLOAT: {
				owlType = OWL2Datatype.XSD_FLOAT;
				break;
			}
			case DOUBLE: {
				owlType = OWL2Datatype.XSD_DOUBLE;
				break;
			}
			case BIGDECIMAL: {
				owlType = OWL2Datatype.XSD_DECIMAL;
				break;
			}
			case BOOLEAN: {
				owlType = OWL2Datatype.XSD_BOOLEAN;
				break;
			}
			case STRING: {
				owlType = OWL2Datatype.XSD_STRING;
				break;
			}
			default: {
				owlType = OWL2Datatype.XSD_STRING;
				break;
			}
		}
		
		return owlType;
	}
	
	public static boolean isClassExtendable(KotlinClass kotlinClass) {
		return kotlinClass.getModifier() != null && (
				kotlinClass.getModifier().equals(ClassModifier.ABSTRACT) ||
				kotlinClass.getModifier().equals(ClassModifier.OPEN) ||
				kotlinClass.getModifier().equals(ClassModifier.SEALED));
	}
	
	public static List<KotlinClass> getSpecificTypeClasses(ClassModifier type, List<KotlinClass> classes) {
		List<KotlinClass> specificClasses = new ArrayList<>();
		for(KotlinClass kotlinClass : classes) {
			if(kotlinClass.getModifier() != null && kotlinClass.getModifier().equals(type)) {
				specificClasses.add(kotlinClass);
			}
		}
		return specificClasses;
	}
	
}
