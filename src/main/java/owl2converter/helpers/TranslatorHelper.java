package owl2converter.helpers;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import commoncodemetamodel.Element;
import commoncodemetamodel.Folder;
import commoncodemetamodel.TextFile;
import owl2converter.generator.OntologyGenerator;
import owl2converter.model.kotlinclass.KotlinClass;
import owl2converter.parser.Parser;

public class TranslatorHelper {
	private static final Logger logger = LoggerFactory.getLogger(TranslatorHelper.class);
	public static void listOutAllKotlinFilesFormCommonCodeMetamodel(List<KotlinClass> classList, Set<Element> elements) {
		for(Element element : elements) {
			translateElement(classList, element);
		}
	}
	
	public static void translateElement(List<KotlinClass> classList, Element element) {
		if(element instanceof Folder) {
			for(Element e : ((Folder) element).getElements()) {
				translateElement(classList, e);
			}
		} else if(element instanceof TextFile) {
			if(((TextFile) element).getExtension() != null && ((TextFile) element).getExtension().equals("kt")) {
				try {
					Parser.parse(classList, ((TextFile) element).getContent());
				} catch (IOException e) {
					logger.error(e.toString());
				}
			}
		}
	}
}
