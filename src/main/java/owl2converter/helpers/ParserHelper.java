package owl2converter.helpers;

import org.antlr.v4.runtime.ParserRuleContext;

import owl2converter.model.field.type.Collection;

public class ParserHelper {
	
	public static String convertType(String type) {
    	String result = "";
    	if(isCollection(type)) {
    		result = type.substring(type.indexOf("<")+1);
    		return result.substring(0, result.indexOf(">"));
    	} 	
    	return type;
    }
    
	public static boolean isCollection(String type) {
    	if(type.contains("<")) {
    		int index = type.indexOf("<");
    		String prefix = type.substring(0, index);
    		return Collection.contains(prefix);
    	}
    	return false;
    }
    
	public static String findClass(ParserRuleContext ctx) {
    	String result = "";
    	if(ctx.getParent()!=null) {
    		for(int i=0; i<ctx.getParent().getChildCount(); i++) {
    			if(ctx.getParent().getChild(i).getText().equals("class")) {
    				result = ctx.getParent().getChild(i+1).getText();
    			}
    		}
    		if(result.isEmpty() && ctx.getParent() != null) {
    			return findClass(ctx.getParent());
    		}
    	}
    	return result;    	
    }
	
}
