package owl2converter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "owl2converter")
public class ApplicationRunner {
	
	public static void main(String[] args) {
		ApplicationContext context = SpringApplication.run(ApplicationRunner.class, args);
	}
	
}
