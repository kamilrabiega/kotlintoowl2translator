package owl2converter.generator;

import java.util.ArrayList;
import java.util.List;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import owl2converter.generator.axioms.ClassAxiomsGenerator;
import owl2converter.generator.axioms.PropertyAxiomsGenerator;
import owl2converter.helpers.GeneratorHelper;
import owl2converter.model.kotlinclass.ClassModifier;
import owl2converter.model.kotlinclass.KotlinClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OntologyGenerator {
	
	private static final Logger logger = LoggerFactory.getLogger(OntologyGenerator.class);
	
	public static List<OWLAxiom> generateOntologyFromClasses(List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		
		axioms.addAll(ClassAxiomsGenerator.createClassesDeclarations(kotlinClasses, owlDataFactory));
		
		List<KotlinClass> abstractClasses = GeneratorHelper.getSpecificTypeClasses(ClassModifier.ABSTRACT, kotlinClasses);
		logger.info("Abstract classes: " + abstractClasses.size());
		axioms.addAll(ClassAxiomsGenerator.createOWLDisjointUnionAxiomForSpecificTypeClasses(abstractClasses, kotlinClasses, owlDataFactory));
		
		List<KotlinClass> sealedClasses = GeneratorHelper.getSpecificTypeClasses(ClassModifier.SEALED, kotlinClasses);
		logger.info("Sealed classes: " + sealedClasses.size());
		axioms.addAll(ClassAxiomsGenerator.createOWLDisjointUnionAxiomForSpecificTypeClasses(sealedClasses, kotlinClasses, owlDataFactory));
		
		axioms.addAll(PropertyAxiomsGenerator.createPropertiesRanges(kotlinClasses, owlDataFactory));
		axioms.addAll(PropertyAxiomsGenerator.createPropertiesDomains(kotlinClasses, owlDataFactory));
		
		return axioms;
	}
	
}
