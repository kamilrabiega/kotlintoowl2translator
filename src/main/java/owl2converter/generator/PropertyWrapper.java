package owl2converter.generator;

public class PropertyWrapper {
	
	private String name;
	private String domain;
	private String range;
	private boolean functional;
	
	public PropertyWrapper(String name, String domain, String range, boolean functional) {
		this.name = name;
		this.domain = domain;
		this.range = range;
		this.functional = functional;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getRange() {
		return range;
	}
	public void setRange(String range) {
		this.range = range;
	}
	public boolean isFunctional() {
		return functional;
	}
	public void setFunctional(boolean functional) {
		this.functional = functional;
	}
	
	@Override
	public String toString() {
		return "PropertyWrapper [name=" + name + ", domain=" + domain + ", range=" + range + ", functional="
				+ functional + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (functional ? 1231 : 1237);
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((range == null) ? 0 : range.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyWrapper other = (PropertyWrapper) obj;
		if (functional != other.functional)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (range == null) {
			if (other.range != null)
				return false;
		} else if (!range.equals(other.range))
			return false;
		return true;
	}
	
	
}
