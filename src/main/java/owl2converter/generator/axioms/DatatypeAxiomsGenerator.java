package owl2converter.generator.axioms;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataOneOf;
import org.semanticweb.owlapi.model.OWLDatatype;
import org.semanticweb.owlapi.model.OWLDatatypeDefinitionAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLLiteral;

public class DatatypeAxiomsGenerator {

	public static OWLDeclarationAxiom createOWLDatatypeAxiom(String dataTypeName, OWLDataFactory owlDataFactory) {
		OWLDatatype owlDataType = owlDataFactory.getOWLDatatype(dataTypeName);
		return owlDataFactory.getOWLDeclarationAxiom(owlDataType);
	}
	
	public static OWLDataOneOf createOWLDataOneOfAxiom(List<String>enumValues, OWLDataFactory owlDataFactory) {
		List<OWLLiteral> values = new ArrayList<>();
		for(String enumValue : enumValues) {
			OWLLiteral owlLiteral = owlDataFactory.getOWLLiteral(enumValue);
			values.add(owlLiteral);
		}
		return owlDataFactory.getOWLDataOneOf(values);
	}
	
	public static OWLDatatypeDefinitionAxiom createDatatypeDefinitionAxiom(String enumName, OWLDataOneOf values, OWLDataFactory owlDataFactory) {
		OWLDatatype owlDataType = owlDataFactory.getOWLDatatype(enumName);
		return owlDataFactory.getOWLDatatypeDefinitionAxiom(owlDataType, values);
	}
	
}
