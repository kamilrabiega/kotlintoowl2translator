package owl2converter.generator.axioms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;

import owl2converter.generator.PropertyWrapper;
import owl2converter.helpers.GeneratorHelper;
import owl2converter.model.field.Field;
import owl2converter.model.field.type.BasicType;
import owl2converter.model.kotlinclass.KotlinClass;

public class PropertyAxiomsGenerator {
	
	public static List<OWLAxiom> createPropertiesRanges(List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		for(KotlinClass kotlinClass : kotlinClasses) {
			if(!kotlinClass.getFields().isEmpty()) {
				for(Field field : kotlinClass.getFields()) {
					if(BasicType.contains(field.getValueType())) {
						axioms.add(DataPropertyAxiomsGenerator.createOWLDataPropertyAxiom(field.getName(), owlDataFactory));
						axioms.add(DataPropertyAxiomsGenerator.createOWLDataPropertyRangeAxiom(field.getName(), field.getValueType(), owlDataFactory));
						axioms.add(owlDataFactory.getOWLSubClassOfAxiom(
											owlDataFactory.getOWLClass(kotlinClass.getName()), 
											DataPropertyAxiomsGenerator.createOWLDataAllValuesFrom(field.getName(), GeneratorHelper.convertBasicType(BasicType.valueOf(field.getValueType().toUpperCase())), owlDataFactory)));
						if(!field.isCollection()) {
							axioms.add(DataPropertyAxiomsGenerator.createOWLFunctionalDataPropertyAxiom(field.getName(), owlDataFactory));
						}
					} else {
						axioms.add(ObjectPropertyAxiomsGenerator.createOWLObjectPropertyAxiom(field.getName(), owlDataFactory));
						for(KotlinClass k : kotlinClasses) {
							if(k.getName().equals(field.getValueType())) {
								if(!GeneratorHelper.isClassExtendable(k)) {
									axioms.add(owlDataFactory.getOWLSubClassOfAxiom(
											owlDataFactory.getOWLClass(kotlinClass.getName()), 
											ObjectPropertyAxiomsGenerator.createOWLObjectAllValuesFrom(field.getName(), field.getValueType(), owlDataFactory)));
								}
							}
						}
						if(!field.isCollection()) {
							axioms.add(ObjectPropertyAxiomsGenerator.createOWLFunctionalObjectPropertyAxiom(field.getName(), owlDataFactory));
						}
						axioms.add(ObjectPropertyAxiomsGenerator.createOWLObjectPropertyRangeAxiom(field.getName(), field.getValueType(), owlDataFactory));
					}
				}
			}
			if(kotlinClass.getOuterClass() != null) {
				axioms.addAll(ObjectPropertyAxiomsGenerator.createHasOuterProperty(kotlinClass.getName(), kotlinClass.getOuterClass().getName(), owlDataFactory));
			}
		}
		return axioms;
	}
	
	public static List<OWLAxiom> createPropertiesDomains(List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		List<PropertyWrapper> objectProperties = new ArrayList<>();
		List<PropertyWrapper> dataProperties = new ArrayList<>();
		for(KotlinClass kotlinClass : kotlinClasses) {
			if(!kotlinClass.getFields().isEmpty()) {
				for(Field field : kotlinClass.getFields()) {
					if(!BasicType.contains(field.getValueType())) {
						objectProperties.add(new PropertyWrapper(field.getName(), 
								kotlinClass.getName(),
								field.getValueType(), 
								!field.isCollection()));
					} else {
						dataProperties.add(new PropertyWrapper(field.getName(), 
								kotlinClass.getName(),
								field.getValueType(), 
								!field.isCollection()));
					}
				}
			}
		}
		
		Map<PropertyWrapper, List<String>> propertiesWithCommonType = new HashMap<>();
		for(PropertyWrapper property : dataProperties) {
			axioms.add(DataPropertyAxiomsGenerator.createOWLDataPropertyDomainAxiom(property.getName(), property.getDomain(), owlDataFactory));
		}
		
		for(PropertyWrapper property : objectProperties) {
			List<String> commonDomain = new ArrayList<>();
			for(PropertyWrapper otherProperty : objectProperties) {
				if(property.getName().equals(otherProperty.getName()) &&
						property.getRange().equals(otherProperty.getRange()) &&
						!property.getDomain().equals(otherProperty.getDomain()) &&
						(property.isFunctional() == otherProperty.isFunctional())) {
					commonDomain.add(otherProperty.getDomain());
				}
			}
			if(propertiesWithCommonType.containsKey(property)) {
				propertiesWithCommonType.get(property).addAll(commonDomain);
			} else {
				propertiesWithCommonType.put(property, commonDomain);
			}
		}
		
		for(Map.Entry<PropertyWrapper, List<String>> entry : propertiesWithCommonType.entrySet()) {
			if(entry.getValue().size() > 1) {
				axioms.add(ObjectPropertyAxiomsGenerator.createOWLObjectPropertyDomainUnionOfAxiom(entry.getKey().getName(), entry.getValue(), owlDataFactory));
			} else {
				axioms.add(ObjectPropertyAxiomsGenerator.createOWLObjectPropertyDomainAxiom(entry.getKey().getName(), entry.getKey().getDomain(), owlDataFactory));
			}
		}
		return axioms;
	}
	
}
