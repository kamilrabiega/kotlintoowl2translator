package owl2converter.generator.axioms;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLDisjointUnionAxiom;
import org.semanticweb.owlapi.model.OWLSubClassOfAxiom;

import owl2converter.model.kotlinclass.ClassModifier;
import owl2converter.model.kotlinclass.KotlinClass;

public class ClassAxiomsGenerator {

	public static OWLDeclarationAxiom createOWLClassAxiom(String className, OWLDataFactory owlDataFactory) {
		OWLClass owlClass = owlDataFactory.getOWLClass(className);
		return owlDataFactory.getOWLDeclarationAxiom(owlClass);
	}

	public static OWLSubClassOfAxiom createOWLSubClassOfAnyAxiom(String className, OWLDataFactory owlDataFactory) {
		OWLClass owlClass = owlDataFactory.getOWLClass(className);
		OWLClass anyClass = owlDataFactory.getOWLClass("Any");
		return owlDataFactory.getOWLSubClassOfAxiom(owlClass, anyClass);
	}
	
	public static OWLSubClassOfAxiom createOWLSubClassOfAxiom(String className, String parentClassName, OWLDataFactory owlDataFactory) {
		OWLClass owlClass = owlDataFactory.getOWLClass(className);
		OWLClass parentOwlClass = owlDataFactory.getOWLClass(parentClassName);
		return owlDataFactory.getOWLSubClassOfAxiom(owlClass, parentOwlClass);
	}
	
	public static OWLDisjointUnionAxiom createOWLDistjointUnionAxiom(OWLClass abstractClass, List<OWLClass> subClasses, OWLDataFactory owlDataFactory) {
		return owlDataFactory.getOWLDisjointUnionAxiom(abstractClass, subClasses);
	}
	
	public static List<OWLAxiom> createOWLDisjointUnionAxiomForSpecificTypeClasses(List<KotlinClass> specificClasses, List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		for(KotlinClass specificClass : specificClasses) {
			List<OWLClass> owlSubClasses = new ArrayList<>();
			for(KotlinClass k : kotlinClasses) {
				if(k.getInheritedClass()!=null && k.getInheritedClass().getName().equals(specificClass.getName())) {
					owlSubClasses.add(owlDataFactory.getOWLClass(k.getName()));
				}
			}
			
			if(!owlSubClasses.isEmpty()) {
				axioms.add(ClassAxiomsGenerator.createOWLDistjointUnionAxiom(owlDataFactory.getOWLClass(specificClass.getName()), owlSubClasses, owlDataFactory));
			}
		}
		return axioms;
	}
	
	public static List<OWLAxiom> createOWLDisjointClassesAxiom(List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		List<OWLClass> owlClasses = new ArrayList<>();
		for(KotlinClass kotlinClass : kotlinClasses) {
			if(kotlinClass.getInheritedClass() == null) {
				if(kotlinClass.getModifier() != null && kotlinClass.getModifier().equals(ClassModifier.ENUM)) {
				} else {
					owlClasses.add(owlDataFactory.getOWLClass(kotlinClass.getName()));
				}
			}
		}
		if(!owlClasses.isEmpty()) {
			axioms.add(owlDataFactory.getOWLDisjointClassesAxiom(owlClasses));
		}
		return axioms;
	}
	
	public static List<OWLAxiom> createClassesDeclarations(List<KotlinClass> kotlinClasses, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		for(KotlinClass kotlinClass : kotlinClasses) {
			if(kotlinClass.getModifier()==null || kotlinClass.getModifier().equals(ClassModifier.OPEN)) {
				axioms.add(ClassAxiomsGenerator.createOWLClassAxiom(kotlinClass.getName(), owlDataFactory));
				if(kotlinClass.getInheritedClass()!=null) {
					axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAxiom(kotlinClass.getName(), 
														kotlinClass.getInheritedClass().getName(),
														owlDataFactory));
				} else {
					axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAnyAxiom(kotlinClass.getName(), owlDataFactory));
				}
			} else if(kotlinClass.getModifier()!=null){
				switch(kotlinClass.getModifier()) {
					case ABSTRACT: {
						//kotlinClass.setName("Abstract"+kotlinClass.getName());
						axioms.add(ClassAxiomsGenerator.createOWLClassAxiom(kotlinClass.getName(), owlDataFactory));
						if(kotlinClass.getInheritedClass()!=null) {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAxiom(kotlinClass.getName(), 
									kotlinClass.getInheritedClass().getName(),
									owlDataFactory));
						} else {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAnyAxiom(kotlinClass.getName(), owlDataFactory));
						}
						
						break;
					}
					case SEALED: {
						axioms.add(ClassAxiomsGenerator.createOWLClassAxiom(kotlinClass.getName(), owlDataFactory));
						if(kotlinClass.getInheritedClass()!=null) {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAxiom(kotlinClass.getName(), 
									kotlinClass.getInheritedClass().getName(),
									owlDataFactory));
						} else {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAnyAxiom(kotlinClass.getName(), owlDataFactory));
						}
						break;
					}
					case DATA: {
						axioms.add(ClassAxiomsGenerator.createOWLClassAxiom(kotlinClass.getName(), owlDataFactory));
						if(kotlinClass.getInheritedClass()!=null) {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAxiom(kotlinClass.getName(), 
									kotlinClass.getInheritedClass().getName(),
									owlDataFactory));
						} else {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAnyAxiom(kotlinClass.getName(), owlDataFactory));
						}
						break;
					}
					case INNER: {
						axioms.add(ClassAxiomsGenerator.createOWLClassAxiom(kotlinClass.getName(), owlDataFactory));
						if(kotlinClass.getInheritedClass()!=null) {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAxiom(kotlinClass.getName(), 
									kotlinClass.getInheritedClass().getName(),
									owlDataFactory));
						} else {
							axioms.add(ClassAxiomsGenerator.createOWLSubClassOfAnyAxiom(kotlinClass.getName(), owlDataFactory));
						}
						break;
					}
					case ENUM: {
						axioms.add(DatatypeAxiomsGenerator.createOWLDatatypeAxiom(kotlinClass.getName(), owlDataFactory));
						axioms.add(DatatypeAxiomsGenerator.createDatatypeDefinitionAxiom(kotlinClass.getName(),
								DatatypeAxiomsGenerator.createOWLDataOneOfAxiom(kotlinClass.getEntries(), owlDataFactory),
																owlDataFactory));
						break;
					}
					default: {
						System.out.println("[WARNING] Class"+ kotlinClass.getName() +"has undefined modifier!");
					}
				}	

			}
		}
		axioms.addAll(createOWLDisjointClassesAxiom(kotlinClasses, owlDataFactory));
		return axioms;
	}
}
