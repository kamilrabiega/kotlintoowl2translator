package owl2converter.generator.axioms;

import java.util.ArrayList;
import java.util.List;

import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalObjectPropertyAxiom;
import org.semanticweb.owlapi.model.OWLObjectAllValuesFrom;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLObjectPropertyRangeAxiom;

public class ObjectPropertyAxiomsGenerator {
	
	public static OWLObjectPropertyDomainAxiom createOWLObjectPropertyDomainUnionOfAxiom(String propertyName, List<String> domains, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		List<OWLClass> union = new ArrayList<>();
		for(String domain : domains) {
			union.add(owlDataFactory.getOWLClass(domain));
		}
		
		return owlDataFactory.getOWLObjectPropertyDomainAxiom(owlObjectProperty, owlDataFactory.getOWLObjectUnionOf(union));
	}
	
	public static List<OWLAxiom> createHasOuterProperty(String domain, String propertyType, OWLDataFactory owlDataFactory) {
		List<OWLAxiom> axioms = new ArrayList<>();
		axioms.add(createOWLObjectPropertyAxiom("hasOuter", owlDataFactory));
		axioms.add(createOWLObjectPropertyDomainAxiom("hasOuter", domain, owlDataFactory));
		axioms.add(createOWLObjectPropertyRangeAxiom("hasOuter", propertyType, owlDataFactory));
		return axioms;
	}
	
	public static OWLDeclarationAxiom createOWLObjectPropertyAxiom(String propertyName, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		return owlDataFactory.getOWLDeclarationAxiom(owlObjectProperty);
	}
	
	public static OWLObjectAllValuesFrom createOWLObjectAllValuesFrom(String propertyName, String propertyType, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		OWLClass range = owlDataFactory.getOWLClass(propertyType);
		return owlDataFactory.getOWLObjectAllValuesFrom(owlObjectProperty, range);
	}
	
	public static OWLObjectPropertyRangeAxiom createOWLObjectPropertyRangeAxiom(String propertyName, String propertyType, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		OWLClass range = owlDataFactory.getOWLClass(propertyType);
		return owlDataFactory.getOWLObjectPropertyRangeAxiom(owlObjectProperty, range);
	}

	public static OWLObjectPropertyDomainAxiom createOWLObjectPropertyDomainAxiom(String propertyName, String domain, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		OWLClass range = owlDataFactory.getOWLClass(domain);
		return owlDataFactory.getOWLObjectPropertyDomainAxiom(owlObjectProperty, range);
	}
	
	public static OWLFunctionalObjectPropertyAxiom createOWLFunctionalObjectPropertyAxiom(String propertyName, OWLDataFactory owlDataFactory) {
		OWLObjectProperty owlObjectProperty = owlDataFactory.getOWLObjectProperty(propertyName);
		return owlDataFactory.getOWLFunctionalObjectPropertyAxiom(owlObjectProperty);
	}
	
}
