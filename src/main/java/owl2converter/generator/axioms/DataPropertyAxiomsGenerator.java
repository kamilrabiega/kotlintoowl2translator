package owl2converter.generator.axioms;

import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataAllValuesFrom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyDomainAxiom;
import org.semanticweb.owlapi.model.OWLDataPropertyRangeAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLFunctionalDataPropertyAxiom;
import org.semanticweb.owlapi.vocab.OWL2Datatype;

import owl2converter.helpers.GeneratorHelper;
import owl2converter.model.field.type.BasicType;

public class DataPropertyAxiomsGenerator {

	public static OWLDeclarationAxiom createOWLDataPropertyAxiom(String propertyName, OWLDataFactory owlDataFactory) {
		OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(propertyName);
		return owlDataFactory.getOWLDeclarationAxiom(owlDataProperty);
	}
	
	public static OWLDataPropertyRangeAxiom createOWLDataPropertyRangeAxiom(String propertyName, String propertyType, OWLDataFactory owlDataFactory) {
		OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(propertyName);
		return owlDataFactory.getOWLDataPropertyRangeAxiom(owlDataProperty, GeneratorHelper.convertBasicType(BasicType.valueOf(propertyType.toUpperCase())));
	}
	
	public static OWLDataPropertyDomainAxiom createOWLDataPropertyDomainAxiom(String propertyName, String domain, OWLDataFactory owlDataFactory) {
		OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(propertyName);
		OWLClass propertyDomain = owlDataFactory.getOWLClass(domain);
		return owlDataFactory.getOWLDataPropertyDomainAxiom(owlDataProperty, propertyDomain);
	}
	
	public static OWLDataAllValuesFrom createOWLDataAllValuesFrom(String propertyName, OWL2Datatype dataType, OWLDataFactory owlDataFactory) {
		OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(propertyName);
		return owlDataFactory.getOWLDataAllValuesFrom(owlDataProperty, dataType);
	}
	
	public static OWLFunctionalDataPropertyAxiom createOWLFunctionalDataPropertyAxiom(String propertyName, OWLDataFactory owlDataFactory) {
		OWLDataProperty owlDataProperty = owlDataFactory.getOWLDataProperty(propertyName);
		return owlDataFactory.getOWLFunctionalDataPropertyAxiom(owlDataProperty);
	}
	
}
