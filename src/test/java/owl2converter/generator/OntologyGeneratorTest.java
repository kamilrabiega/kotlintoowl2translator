package owl2converter.generator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntologyManager;

import owl2converter.model.field.Field;
import owl2converter.model.kotlinclass.ClassModifier;
import owl2converter.model.kotlinclass.KotlinClass;

public class OntologyGeneratorTest {
	
	private static final OWLOntologyManager ontologyManager = OWLManager.createConcurrentOWLOntologyManager();
	private static final OWLDataFactory owlDataFactory = ontologyManager.getOWLDataFactory();
	private List<KotlinClass> kotlinClasses;
	private List<OWLAxiom> axioms;
	
	/*
	 * T1. Transformacja prostych klas
	 */
	@Test
	public void transltionOfSimpleKotlinClass() {
		String actualResult = "";
		KotlinClass simpleClass = new KotlinClass();
		simpleClass.setName("SimpleClass");
		kotlinClasses.add(simpleClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.SIMPLE_CLASS, actualResult);
	}
	
	/*
	 * TODO: T2. Transformacja dziedziczenia wszystkich klas po klasie Any
	 */
	@Test
	public void translationAnyInheritance() {
		String actualResult = "";
		KotlinClass firstClass = new KotlinClass();
		KotlinClass secondClass = new KotlinClass();
		firstClass.setName("FirstClass");
		secondClass.setName("SecondClass");
		kotlinClasses.add(firstClass);
		kotlinClasses.add(secondClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.ANY_INHERITANCE, actualResult);
	}
	
	
	/*
	 * T3. Transformacja pól klas do właściwości
	 */
	@Test
	public void translationOfClassWithFields() {
		String actualResult = "";
		KotlinClass exampleClass = new KotlinClass();
		exampleClass.setName("ExampleClass");
		Field singleField = new Field("singleField", "String", false);
		Field collectionField = new Field("collectionField", "Int", true);
		exampleClass.setFields(Arrays.asList(singleField, collectionField));
		kotlinClasses.add(exampleClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.CLASS_WITH_FIELDS, actualResult);
	}
	
	/*
	 * T5. Transformacja generalizacji
	 */
	@Test
	public void translationOfClassInheritance() {
		String actualResult = "";
		KotlinClass parentClass = new KotlinClass();
		parentClass.setName("ParentClass");
		KotlinClass subClass = new KotlinClass();
		subClass.setName("SubClass");
		subClass.setInheritedClass(parentClass);
		kotlinClasses.add(parentClass);
		kotlinClasses.add(subClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.INHERITANCE, actualResult);
	}

	/*
	 * T6. Transformacja klas abstrakcyjnych	 
	 */	
	@Test
	public void translationOfAbstractClass() {
		String actualResult = "";
		KotlinClass abstractClass = new KotlinClass();
		abstractClass.setName("AbstractClass");
		abstractClass.setModifier(ClassModifier.ABSTRACT);
		kotlinClasses.add(abstractClass);
		
		KotlinClass firstSubClass = new KotlinClass();
		firstSubClass.setName("FirstSubClass");
		firstSubClass.setInheritedClass(abstractClass);
		kotlinClasses.add(firstSubClass);
		
		KotlinClass secondSubClass = new KotlinClass();
		secondSubClass.setName("SecondSubClass");
		secondSubClass.setInheritedClass(abstractClass);
		kotlinClasses.add(secondSubClass);
		
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.ABSTRACT_CLASS, actualResult);
	}
	
	/*
	 * 	T7. Transformacja typów wyliczeniowych
	 */
	@Test
	public void translationOfSimpleEnum() {
		String actualResult = "";
		KotlinClass simpleEnum = new KotlinClass();
		simpleEnum.setName("SimpleEnum");
		simpleEnum.setModifier(ClassModifier.ENUM);
		simpleEnum.setEntries(Arrays.asList("FirstValue","SecondValue"));
		kotlinClasses.add(simpleEnum);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.ENUMERATOR, actualResult);
	}
	
	/*
	 * TODO: T8. Transformacja klas osadzonych i wewnętrznych
	 */
	@Test
	public void translationOfInnerClass() {
		String actualResult = "";
		KotlinClass outerClass = new KotlinClass();
		KotlinClass innerClass = new KotlinClass();
		outerClass.setName("OuterClass");
		innerClass.setName("InnerClass");
		innerClass.setOuterClass(outerClass);
		innerClass.setModifier(ClassModifier.INNER);
		kotlinClasses.add(outerClass);
		kotlinClasses.add(innerClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.INNER_CLASS, actualResult);
	}
	
	/*
	 * TODO: T9. Transformacja klas data
	 */
	@Test
	public void transltionOfDataKotlinClass() {
		String actualResult = "";
		KotlinClass dataClass = new KotlinClass();
		dataClass.setName("DataClass");
		kotlinClasses.add(dataClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.DATA_CLASS, actualResult);
	}
	
	/*
	 * TODO: T10. Transformacja szczelnej hierarchii
	 */
	@Test
	public void translationOfSealedClass() {
		String actualResult = "";
		KotlinClass sealedClass = new KotlinClass();
		sealedClass.setName("SealedClass");
		sealedClass.setModifier(ClassModifier.SEALED);
		kotlinClasses.add(sealedClass);
		
		KotlinClass firstSubClass = new KotlinClass();
		firstSubClass.setName("FirstSubClass");
		firstSubClass.setInheritedClass(sealedClass);
		kotlinClasses.add(firstSubClass);
		
		KotlinClass secondSubClass = new KotlinClass();
		secondSubClass.setName("SecondSubClass");
		secondSubClass.setInheritedClass(sealedClass);
		kotlinClasses.add(secondSubClass);
		
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.SEALED_CLASS, actualResult);
	}
	
	/*
	 * TODO: T11. Generowanie domeny właściwości będącej sumą klas
	 */
	@Test
	public void translationOfFieldWithCommonDomain() {
		String actualResult = "";
		KotlinClass firstClass = new KotlinClass();
		firstClass.setName("FirstClass");
		KotlinClass secondClass = new KotlinClass();
		secondClass.setName("SecondClass");
		Field fieldWithCommonDomain = new Field("singleField", "FirstClass", false);
		firstClass.setFields(Arrays.asList(fieldWithCommonDomain));
		secondClass.setFields(Arrays.asList(fieldWithCommonDomain));
		kotlinClasses.add(firstClass);
		kotlinClasses.add(secondClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.FIELD_WITH_COMMON_DOMAIN, actualResult);
	}
	
	/*
	 * TODO: T12. Generowanie aksjomatów Object/DataAllValuesFrom 
	 */
	@Test
	public void translationOfFieldWithAllValuesFrom() {
		String actualResult = "";
		KotlinClass exampleClass = new KotlinClass();
		exampleClass.setName("ExampleClass");
		KotlinClass anotherClass = new KotlinClass();
		exampleClass.setName("AnotherClass");
		Field dataField = new Field("dataField", "String", false);
		Field objectField = new Field("objectField", "AnotherClass", false);
		exampleClass.setFields(Arrays.asList(dataField, objectField));
		kotlinClasses.add(exampleClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.FIELD_ALL_VALUES_FROM, actualResult);
	}
	
	/*
	 * TODO: T13. Transformacja rozłączności klas
	 */
	@Test
	public void transltionOfDisjointClass() {
		String actualResult = "";
		KotlinClass simpleClass = new KotlinClass();
		simpleClass.setName("SimpleClass");
		KotlinClass anotherSimpleClass = new KotlinClass();
		anotherSimpleClass.setName("AnotherSimpleClass");
		kotlinClasses.add(simpleClass);
		kotlinClasses.add(anotherSimpleClass);
		axioms = OntologyGenerator.generateOntologyFromClasses(kotlinClasses, owlDataFactory);
		actualResult = convertGeneratedResult(axioms);
		assertEquals(Constants.DISJOINT_CLASSES, actualResult);
	}
	
	@Before
	public void init() {
		kotlinClasses = new ArrayList<>();
		axioms = new ArrayList<>();
	}
	
	public String convertGeneratedResult(List<OWLAxiom> axioms) {
		String actualResult = "";
		for(int i=0; i<axioms.size(); i++) {
			// Remove autogenerated OWL API axiom
			if(!axioms.get(i).toString().contains("Generated by the OWL API")){
				actualResult = actualResult + axioms.get(i).toString() + "\n";
			}
		}
		return actualResult;
	}
	
}
