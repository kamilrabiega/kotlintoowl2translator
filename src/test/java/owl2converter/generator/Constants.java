package owl2converter.generator;

public class Constants {
	public static final String INHERITANCE = 
			"Declaration(Class(<ParentClass>))\n" + 
			"SubClassOf(<ParentClass> <Any>)\n" + 
			"Declaration(Class(<SubClass>))\n" + 
			"SubClassOf(<SubClass> <ParentClass>)\n";
	
	public static final String SIMPLE_CLASS = 
			"Declaration(Class(<SimpleClass>))\n" + 
			"SubClassOf(<SimpleClass> <Any>)\n";
	
	public static final String DATA_CLASS = 
			"Declaration(Class(<DataClass>))\n" + 
			"SubClassOf(<DataClass> <Any>)\n";
	
	public static final String CLASS_WITH_FIELDS = 
			"Declaration(Class(<ExampleClass>))\n" + 
			"SubClassOf(<ExampleClass> <Any>)\n" + 
			"Declaration(DataProperty(<singleField>))\n" + 
			"DataPropertyRange(<singleField> xsd:string)\n" + 
			"SubClassOf(<ExampleClass> DataAllValuesFrom(<singleField> xsd:string))\n" + 
			"FunctionalDataProperty(<singleField>)\n" + 
			"Declaration(DataProperty(<collectionField>))\n" + 
			"DataPropertyRange(<collectionField> xsd:int)\n" + 
			"SubClassOf(<ExampleClass> DataAllValuesFrom(<collectionField> xsd:int))\n" + 
			"DataPropertyDomain(<singleField> <ExampleClass>)\n" + 
			"DataPropertyDomain(<collectionField> <ExampleClass>)\n";
	
	
	public static final String ENUMERATOR = 
			"Declaration(Datatype(<SimpleEnum>))\n" + 
			"DatatypeDefinition(<SimpleEnum> DataOneOf(\"FirstValue\"^^xsd:string \"SecondValue\"^^xsd:string ))\n";
	
	public static final String ABSTRACT_CLASS = 
			"Declaration(Class(<AbstractClass>))\n" + 
			"SubClassOf(<AbstractClass> <Any>)\n" + 
			"Declaration(Class(<FirstSubClass>))\n" + 
			"SubClassOf(<FirstSubClass> <AbstractClass>)\n" + 
			"Declaration(Class(<SecondSubClass>))\n" + 
			"SubClassOf(<SecondSubClass> <AbstractClass>)\n" + 
			"DisjointUnion(<AbstractClass> <FirstSubClass> <SecondSubClass> )\n";
	
	public static final String ANY_INHERITANCE = 
			"Declaration(Class(<FirstClass>))\n" + 
			"SubClassOf(<FirstClass> <Any>)\n" + 
			"Declaration(Class(<SecondClass>))\n" + 
			"SubClassOf(<SecondClass> <Any>)\n" + 
			"DisjointClasses(<FirstClass> <SecondClass>)\n"; 
	
	public static final String INNER_CLASS = 
			"Declaration(Class(<OuterClass>))\n" + 
			"SubClassOf(<OuterClass> <Any>)\n" + 
			"Declaration(Class(<InnerClass>))\n" + 
			"SubClassOf(<InnerClass> <Any>)\n" + 
			"DisjointClasses(<InnerClass> <OuterClass>)\n" + 
			"Declaration(ObjectProperty(<hasOuter>))\n" + 
			"ObjectPropertyDomain(<hasOuter> <InnerClass>)\n" + 
			"ObjectPropertyRange(<hasOuter> <OuterClass>)\n"; 
	
	public static final String SEALED_CLASS = 
			"Declaration(Class(<SealedClass>))\n" + 
			"SubClassOf(<SealedClass> <Any>)\n" + 
			"Declaration(Class(<FirstSubClass>))\n" + 
			"SubClassOf(<FirstSubClass> <SealedClass>)\n" + 
			"Declaration(Class(<SecondSubClass>))\n" + 
			"SubClassOf(<SecondSubClass> <SealedClass>)\n" + 
			"DisjointUnion(<SealedClass> <FirstSubClass> <SecondSubClass> )\n";
	
	public static final String FIELD_WITH_COMMON_DOMAIN = 
			"Declaration(Class(<FirstClass>))\n" + 
			"SubClassOf(<FirstClass> <Any>)\n" + 
			"Declaration(Class(<SecondClass>))\n" + 
			"SubClassOf(<SecondClass> <Any>)\n" + 
			"DisjointClasses(<FirstClass> <SecondClass>)\n" + 
			"Declaration(ObjectProperty(<singleField>))\n" + 
			"SubClassOf(<FirstClass> ObjectAllValuesFrom(<singleField> <FirstClass>))\n" + 
			"FunctionalObjectProperty(<singleField>)\n" + 
			"ObjectPropertyRange(<singleField> <FirstClass>)\n" + 
			"Declaration(ObjectProperty(<singleField>))\n" + 
			"SubClassOf(<SecondClass> ObjectAllValuesFrom(<singleField> <FirstClass>))\n" + 
			"FunctionalObjectProperty(<singleField>)\n" + 
			"ObjectPropertyRange(<singleField> <FirstClass>)\n" + 
			"ObjectPropertyDomain(<singleField> ObjectUnionOf(<FirstClass> <SecondClass>))\n";
	
	public static final String FIELD_ALL_VALUES_FROM = 
			"Declaration(Class(<AnotherClass>))\n" + 
			"SubClassOf(<AnotherClass> <Any>)\n" + 
			"Declaration(DataProperty(<dataField>))\n" + 
			"DataPropertyRange(<dataField> xsd:string)\n" + 
			"SubClassOf(<AnotherClass> DataAllValuesFrom(<dataField> xsd:string))\n" + 
			"FunctionalDataProperty(<dataField>)\n" + 
			"Declaration(ObjectProperty(<objectField>))\n" + 
			"SubClassOf(<AnotherClass> ObjectAllValuesFrom(<objectField> <AnotherClass>))\n" + 
			"FunctionalObjectProperty(<objectField>)\n" + 
			"ObjectPropertyRange(<objectField> <AnotherClass>)\n" + 
			"DataPropertyDomain(<dataField> <AnotherClass>)\n" + 
			"ObjectPropertyDomain(<objectField> <AnotherClass>)\n";
	
	public static final String DISJOINT_CLASSES = 
			"Declaration(Class(<SimpleClass>))\n" + 
			"SubClassOf(<SimpleClass> <Any>)\n" + 
			"Declaration(Class(<AnotherSimpleClass>))\n" + 
			"SubClassOf(<AnotherSimpleClass> <Any>)\n" + 
			"DisjointClasses(<AnotherSimpleClass> <SimpleClass>)\n";
}